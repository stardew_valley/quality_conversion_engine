#! /usr/bin/env python

import json
import copy

items = {}
template = {}

with open("items.txt", "r") as f:
    lines = f.readlines()

with open("template-ProducerRules-deterministic.json", "r") as f:
    template["deterministic"] = json.loads(f.read())

with open("template-ProducerRules-stochastic.json", "r") as f:
    template["stochastic"] = json.loads(f.read())

rules = {}
rules["deterministic"] = []
rules["stochastic"] = []

QUALITIES = {
    1: "quality_silver",
    2: "quality_gold",
    4: "quality_iridium"
}

def DET(pow, item):
    index = 2**pow
    numInputs = 4 // index
    numOutputs = numInputs + 1
    numMinutes = 30 * numInputs
    additional_output = {
        "OutputIdentifier": item,
        "OutputProbability": 1,
        "RequiredQuality": [index],
        "MinutesUntilReady": numMinutes,
        "RequiredInputStack": numInputs,
        "OutputStack": numOutputs
    }
    return additional_output

for line in lines:
    item = line.rstrip()
    tmp_det = copy.deepcopy(template["deterministic"])
    tmp_det = {
        **template["deterministic"],
        **{"InputIdentifier": item, "OutputIdentifier": item}
    }
    # tmp_det["AdditionalOutputs"] = []
    #for k in range(3):
        #tmp_det["AdditionalOutputs"].append(DET(k, item))
    
    rules["deterministic"].append(tmp_det)

    tmp_sto = copy.deepcopy(template["stochastic"])
    tmp_sto["InputIdentifier"] = item
    tmp_sto["OutputIdentifier"] = item

    rules["stochastic"].append(tmp_sto)

for i in ("deterministic", "stochastic"):
    with open(f"ProducerRules-{i}.json", "x") as f:
        f.write(json.dumps(rules[i], sort_keys=True, indent=4))