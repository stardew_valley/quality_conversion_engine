all: zip clean

clean:
	rm -rf "[CP] Quality Conversion Engine"
	echo "Filesystem cleaned"

zip: files
	rm -f "[CP] Quality Conversion Engine.zip"
	zip -r "[CP] Quality Conversion Engine.zip" "[CP] Quality Conversion Engine/"
	echo "Zip file complete!"

files: folders
	cp LICENCE "[CP] Quality Conversion Engine/"
	cp README.txt "[CP] Quality Conversion Engine/"
	cp manifest.json "[CP] Quality Conversion Engine/"
	cp content.json "[CP] Quality Conversion Engine/"
	cp -r "assets/" "[CP] Quality Conversion Engine/"
	cp -r "simple/"*.json "[CP] Quality Conversion Engine/simple/"

folders:
	mkdir -p "[CP] Quality Conversion Engine/assets"
	mkdir -p "[CP] Quality Conversion Engine/simple"
