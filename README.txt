Quality Conversion Engine - 2.1.4
Provides a machine that downgrades primary produce (crops and tree fruits, forage, animal products, fish, certain seeds) and increases the quantity commensurately.

:: CHANGE LOG::
• 2.1.4 Include beachcombing forage, such as Coral, Nautilus Shells and Rainbow Shells
• 2.1.2 Include Dinosaur Eggs
• 2.1.1 Add Junimatic compatibility and adjust animation duration to cover gap (hopefully)
• 2.1.0 Add options to control cost and processing speed. Default behaviour is original cost but much slower speed.
• 2.0.1 Add in option to normalise vanilla machines, eliminating quality-dependent mechanics. (Enabled by default.)

:: REQUIREMENTS::
• Content Patcher
• Optionally, Generic Mod Config Menu

:: FEATURES::
• Conserves total value of the primary produce on average.
• Blocks input of Normal quality items, so automated setups don't result in constant cycling.
• Silver quality items convert to 1 (75%) or 2 (25%) Normal quality items.
• Gold quality items convert to 1 (50%) or 2 (50%) Normal quality items.
• Iridium quality items convert to 2 Normal quality items.

:: CREDITS::
• Artwork from Traktori's Quality Scrubber. Used under general permission.
• Initial machine logic from mikemonroe.
